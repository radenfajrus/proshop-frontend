import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import { 
    productListReducer, 
    productDetailReducer, 
} from './reducers/productReducer'
import { 
    cartReducer, 
} from './reducers/cartReducer'
import { 
    userLoginReducer, 
    userRegisterReducer, 
    userDetailReducer,
    userUpdateProfileReducer,
} from './reducers/userReducer'
import {
    orderCreateReducer, 
    orderDetailReducer,
    orderHistoryReducer,
    orderPayReducer,
} from './reducers/orderReducer'

const reducer = combineReducers({
    productList: productListReducer,
    productListDetail: productDetailReducer,
    cart: cartReducer,
    userLogin: userLoginReducer,
    userRegister: userRegisterReducer,
    userDetail: userDetailReducer,
    userUpdateProfile: userUpdateProfileReducer,
    orderCreate: orderCreateReducer,
    orderDetail: orderDetailReducer,
    orderPay: orderPayReducer,
    orderHistory: orderHistoryReducer,
})

const cartItemsFromStorage = localStorage.getItem(`cartItems`) ? JSON.parse(localStorage.getItem('cartItems')) : []
const userInfoFromStorage = localStorage.getItem('userInfo') ? JSON.parse(localStorage.getItem('userInfo')) : null
const shippingAddressFromStorage = localStorage.getItem(`shippingAddress`) ? JSON.parse(localStorage.getItem('shippingAddress')) : {}
const paymentMethodFromStorage = localStorage.getItem(`paymentMethod`) ? JSON.parse(localStorage.getItem('paymentMethod')) : null


const initialState = {
    cart : {
        cartItems: cartItemsFromStorage,
        shippingAddress: shippingAddressFromStorage,
        paymentMethod: paymentMethodFromStorage,
    },
    userLogin : {userInfo: userInfoFromStorage},
}
const middleware = [thunk]

const store = createStore(
    reducer, 
    initialState, 
    composeWithDevTools(
        applyMiddleware(...middleware)
    )
)


export default store
