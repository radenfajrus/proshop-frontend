import React from 'react'
import Input from './Input'

export default {
    title: 'Items/Input',
    component: Input,
}

export const TextSmall = () => (
    <Input size='sm' type='text' placeholder='Text Small'>Text Small</Input>
)
export const TextMedium = () => (
    <Input size='md' type='text' placeholder='Text Medium'>Text Medium</Input>
)
export const TextLarge = () => (
    <Input size='lg' type='text' placeholder='Text Large'>Text Large</Input>
)