import React from 'react'
import './Input.css'
import { InputGroup,FormControl } from 'react-bootstrap'

const Input = (props) => {
    const { size = 'md', type = 'text', placeholder='', children, ...rest } = props

    return (
        <div>
        <InputGroup size={`${size}`}>
            <InputGroup.Prepend>
            <InputGroup.Text id={`inputGroup-sizing-${size}`}>{`${placeholder}`}</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl aria-label={`${placeholder}`} aria-describedby={`inputGroup-sizing-${size}`} />
        </InputGroup>
        </div>
    )
}

export default Input
