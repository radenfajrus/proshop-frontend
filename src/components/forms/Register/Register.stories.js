import React from 'react'
import Button from '../../items/Button/Button'

export default {
    title: 'Form/Register',
    component: Button,
}

export const RegisterNewUser = () => <Button variant='primary'>Register New User</Button>
export const RegisterNewSubscription = () => <Button variant='secondary'>Register New Subscription</Button>