import React, { useEffect } from 'react'
import { Row, Col } from 'react-bootstrap'
import Product from '../components/Product'
import Message from '../components/Message'
import Loader from '../components/Loader'
import FormContainer from '../components/FormContainer'
import { useDispatch, useSelector } from 'react-redux'
import { listProduct } from '../actions/productAction.js'


const HomeScreen = () => {
    const dispatch = useDispatch()

    const productList = useSelector(state => state.productList)
    const { loading, error, products } = productList

    useEffect(()=>{
        dispatch(listProduct())
    }, [dispatch])

    // const products = []

    return (
        <>
        <h1>Latest Products</h1>
        {
        loading ? <Loader></Loader>: 
        error ? <Message variant='danger' >{error}</Message>: 
            <Row>
                {products.map(product => { return <>
                    <Col sm={12} md={6} lg={4} xl={3}>
                        <Product product={product}></Product>
                    </Col>
                </>})}
            </Row>
        }
        </>
    )
}

export default HomeScreen
