import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Form, Col, Button } from 'react-bootstrap'
import FormContainer from '../components/FormContainer'
import CheckoutStep from '../components/CheckoutStep'
import {savePaymentMethod} from '../actions/cartAction'


const PaymentScreen = ({history}) => {
    const cart = useSelector((state) => state.cart)
    const {shippingAddress} = cart

    let defaultPaymentMethod = cart.paymentMethod

    if(!shippingAddress){
        history.push('/shipping')
    }

    const [paymentMethod, setPaymentMethod] = useState(defaultPaymentMethod)

    const dispatch = useDispatch()

    const submitHandler = (e) => {
        e.preventDefault()
        dispatch(savePaymentMethod(paymentMethod))
        history.push('/placeorder')
    }

    return (
        <FormContainer>
            <CheckoutStep step1 step2 step3></CheckoutStep>
            <h1>Payment Method</h1>
            <Form onSubmit={submitHandler}>
                <Form.Group controlId='paymentMethod'>
                    <Form.Label>Select Method</Form.Label>
                    <Col>
                        <Form.Check 
                            type='radio' 
                            label='Paypal or Credit Card' 
                            id='Paypal' 
                            name='paymentMethod' 
                            value='Paypal' 
                            checked={paymentMethod === 'Paypal'}
                            onChange={e=>setPaymentMethod(e.target.value)}
                        >
                        </Form.Check>
                        <Form.Check 
                            type='radio' 
                            label='Stripe' 
                            id='Stripe' 
                            name='paymentMethod' 
                            value='Stripe' 
                            checked={paymentMethod === 'Stripe'}
                            onChange={e=>setPaymentMethod(e.target.value)}
                        >
                        </Form.Check>
                    </Col>
                </Form.Group>
                <Button type='submit' variant='primary'>Continue</Button>
            </Form>
        </FormContainer>
    )
}

export default PaymentScreen
