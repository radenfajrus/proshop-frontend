import axios from 'axios'

import {
    ORDER_CREATE_REQUEST,
    ORDER_CREATE_SUCCESS,
    ORDER_CREATE_FAIL,
    ORDER_DETAIL_REQUEST,
    ORDER_DETAIL_SUCCESS,
    ORDER_DETAIL_FAIL,
    ORDER_PAY_REQUEST,
    ORDER_PAY_SUCCESS,
    ORDER_PAY_FAIL,
    ORDER_HISTORY_REQUEST,
    ORDER_HISTORY_SUCCESS,
    ORDER_HISTORY_FAIL,
} from '../constants/orderConstant'

export const createOrder = (order) => async (dispatch, getState) => {
    try {
        dispatch({type: ORDER_CREATE_REQUEST })

        const { userLogin: { userInfo } } = getState()

        const config = {
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${userInfo.token}`
            }
        }

        const { data } = await axios.post(`/api/orders`, order, config)

        dispatch({
            type: ORDER_CREATE_SUCCESS,
            payload: data,
        })
    
    } catch (error) {
        dispatch({
            type: ORDER_CREATE_FAIL,
            payload: error.response && error.response.data.message ? error.response.data.message : error.message,
        })

    }
}

export const getOrderDetail = (id) => async (dispatch,getState) => {
    try {
        dispatch({type: ORDER_DETAIL_REQUEST })

        const { userLogin: { userInfo } } = getState()

        const config = {
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${userInfo.token}`
            }
        }

        const { data } = await axios.get(`/api/orders/${id}`, config)

        dispatch({
            type: ORDER_DETAIL_SUCCESS,
            payload: data
        })

    } catch (error) {
        dispatch({
            type: ORDER_DETAIL_FAIL ,
            payload: error.response && error.response.data.message ? error.response.data.message : error.message,
        })

    }
}

export const getOrderHistory = () => async (dispatch,getState) => {
    try {
        dispatch({type: ORDER_HISTORY_REQUEST })

        const { userLogin: { userInfo } } = getState()

        const config = {
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${userInfo.token}`
            }
        }

        const { data } = await axios.get(`/api/orders/history`, config)

        dispatch({
            type: ORDER_HISTORY_SUCCESS,
            payload: data
        })

    } catch (error) {
        dispatch({
            type: ORDER_HISTORY_FAIL ,
            payload: error.response && error.response.data.message ? error.response.data.message : error.message,
        })

    }
}

export const payOrder = (orderId, paymentResult) => async (dispatch, getState) => {
    try {
        dispatch({type: ORDER_PAY_REQUEST })

        const { userLogin: { userInfo } } = getState()

        const config = {
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${userInfo.token}`
            }
        }

        const { data } = await axios.put(`/api/orders/${orderId}/pay`, paymentResult, config)

        dispatch({
            type: ORDER_PAY_SUCCESS,
            payload: data
        })

    } catch (error) {
        dispatch({
            type: ORDER_PAY_FAIL,
            payload: error.response && error.response.data.message ? error.response.data.message : error.message,
        })

    }
}


